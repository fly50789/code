#
# @lc app=leetcode id=1 lang=python3
#
# [1] Two Sum
#
from typing import List

# @lc code=start
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:

        for index_1,i in enumerate(nums[:-1]):
                if  target-i in nums[index_1+1:]:
                    #print(i,j,target,index_list[index_1],index_list[index_1+index_2+1])
                    return [index_1,index_1+nums[index_1+1:].index(target-i)+1]



# @lc code=end

def test_1():
    nums = [1,2,3]
    target = 4
    result = Solution().twoSum(nums,target)
    assert [0,2] == result,\
        'result:{}'.format(result)

def test_2():
    nums = [0,4,3,0]
    target = 0
    result = Solution().twoSum(nums,target)
    assert [0,3] == result,\
        'result:{}'.format(result)
if __name__ =='__main__':
    test_1()
    test_2()
