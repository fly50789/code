> (Title name format: [Project-name](title-name)) <br>
> i.e. [SVP] automatic validation SOP document  <br>
> i.e. [SRE] Grafana monitor dashboard user guide <br>
---


# Objective 
###### (Specfic document description in detail, i.e. Add Unittest stage in gitlab-ci pipeline)
- 


---
> **(Please follow the procedure below to set this items in the issue)** <br>
> Assignee: (use **/assign @name** to set assignee of this feature) <br>
/assign  
> Due Date: (use **/due date** to set task's deadline i.e. in 2 days | this Friday | December 1st) <br>
/due  
> Milestone: (use **/milestone %milestone** to set dedicated milestone) <br>
/milestone 
> Label： (use **/label ~label** to set label on issue, need label: assignee-name & issue-type) <br>
/label 
> Estimated time: (use **/estimate time** to set estimated developed time i.e. 1w 2d 5h) <br>
/estimate
> Spend time: (during the development, please use **/spend time** to take spend time note i.e. 2d 5h) <br>
