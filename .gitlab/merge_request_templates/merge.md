> (Title name format: [Project-name](title-name)-(phase-number:optional)) <br>
> (if issue contains multiple phase, please create 1 major issue and  N sub-issues) <br>
> i.e. [SVP] Test script editor page <br>
> i.e. [SVP] Test script editor page - phase 1 <br>
> i.e. [SVP] Test script editor page - phase 2 <br>
---


# Objective 
###### (Specfic completed function description in detail in top-down list, i.e. Complete Test script editor backend API)
- 


# Change items
###### (Simply description of the code changes i.e. modify getUser() function in commonApi.js)
- 

---
> **(Please follow the procedure below to set this items in the issue)** <br>
> Assignee: (use **/assign @name** to set assignee of this merge request) <br>
/assign 
> Reviewer: (use **/assign_reviewer @name** to set reviewer of this merge request) <br>
/assign_reviewer
> Milestone: (use **/milestone %milestone** to set dedicated milestone) <br>
/milestone 
> Label： (use **/label ~label** to set label on issue, need label: issue-type & status) <br>
/label 
